'use strict';
var async = require('async');

var blacklistRegex = [
  /.*\.spamtrail\.com/i,
  /.*\.mailexpire\.com/i,
  /.*@yopmail\.com/i,
  /.*@willhackforfood\.biz/i,
  /.*@wegwerfmail\.org/i,
  /.*@wegwerfmail\.net/i,
  /.*@wegwerfmail\.de/i,
  /.*@wegwerfadresse\.de/i,
  /.*@wegwerf-email\.net/i,
  /.*@watchfull\.net/i,
  /.*@watch-harry-potter\.com/i,
  /.*@trashmail\.ws/i,
  /.*@trashmail\.net/i,
  /.*@trashmail\.me/i,
  /.*@trashmail\.de/i,
  /.*@trashmail\.com/i,
  /.*@trashmail\.at/i,
  /.*@trashemail\.de/i,
  /.*@trash2009\.com/i,
  /.*@trash-mail\.com/i,
  /.*@trash-mail\.at/i,
  /.*@tempemail\.net/i,
  /.*@teewars\.org/i,
  /.*@superstachel\.de/i,
  /.*@super-auswahl\.de/i,
  /.*@spammotel\.com/i,
  /.*@spamgourmet\.com/i,
  /.*@spamex\.com/i,
  /.*@spambog\.ru/i,
  /.*@spambog\.de/i,
  /.*@spambog\.com/i,
  /.*@spam\.la/i,
  /.*@sofort-mail\.de/i,
  /.*@snkmail\.com/i,
  /.*@sneakemail\.com/i,
  /.*@sandelf\.de/i,
  /.*@s0ny\.net/i,
  /.*@recode\.me/i,
  /.*@rcpt\.at/i,
  /.*@proxymail\.eu/i,
  /.*@owlpic\.com/i,
  /.*@objectmail\.com/i,
  /.*@nurfuerspam\.de/i,
  /.*@nomail2me\.com/i,
  /.*@nervtmich\.net/i,
  /.*@nervmich\.net/i,
  /.*@misterpinball\.de/i,
  /.*@mailnull\.com/i,
  /.*@mailinator\.com/i,
  /.*@mail4trash\.com/i,
  /.*@kurzepost\.de/i,
  /.*@kulturbetrieb\.info/i,
  /.*@jetable\.com/i,
  /.*@imails\.info/i,
  /.*@hulapla\.de/i,
  /.*@hochsitze\.com/i,
  /.*@guerillamail\.or/i,
  /.*@great-host\.in/i,
  /.*@geschent\.biz/i,
  /.*@film-blog\.biz/i,
  /.*@es4\.de/i,
  /.*@ero-tube\.org/i,
  /.*@emailias\.com/i,
  /.*@edv\.to/i,
  /.*@dontsendmespam\.de/i,
  /.*@discardmail\.de/i,
  /.*@discardmail\.com/i,
  /.*@despammed\.com/i,
  /.*@dbunker\.com/i,
  /.*@cust\.in/i,
  /.*@bund\.us/i,
  /.*@brennendesreich\.de/i,
  /.*@bio-muesli\.net/i,
  /.*@bio-muesli\.info/i,
  /.*@antichef\.net/i,
  /.*@ano-mail\.net/i,
  /.*@SmellFear\.com/i,
  /.*@LookUgly\.com/i,
  /.*@FudgeRub\.com/i,
  /.*@DingBone\.com/i,
  /.*@BeefMilk\.com/i,
  /.*@3d-painting\.com/i,
  /.*@10minutemail\.com/i,
  /.*@0815\.ru/i
];

module.exports = function (message) {
  message = message || ('required');

  return {
    validator: function (value, next) {
      async.eachSeries(
        blacklistRegex,
        function (regex, callback) {
          if (regex.test(value)) {
            return callback('Blacklist Hit');
          }
          callback();
        },
        function (err) {
          if (err) {
            next(false);
          }

          next(true);
        }
      );
    },
    msg: message
  };
};