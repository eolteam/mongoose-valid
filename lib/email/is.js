'use strict';
module.exports = function isEmail(message) {
  message = message || ('required');

  return {
    validator: function (value, next) {
      next(
        /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value)
      );
    },
    msg: message
  };
};