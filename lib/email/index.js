'use strict';

module.exports = {
  is: require('./is.js'),
  isTrash: require('./isTrash.js')
};