'use strict';

var lodash = require('lodash');

module.exports = function validateRequired(message) {
  message = message || 'Field is required.';

  return {
    validator: function (value, next) {
      next(
        !lodash.isUndefined(value) && !lodash.isNull(value) && !lodash.isEmpty(value)
      );
    },
    msg: message
  };
};