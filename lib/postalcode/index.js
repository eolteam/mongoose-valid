'use strict';

var lodash = require('lodash');
var validators = require('./validators.js');

module.exports = function (message, defaultCountryCode, countryCodeField) {
  message = message || ('Value is not a valid postalcode.');
  defaultCountryCode = defaultCountryCode || 'none';

  return {
    validator: function (value, next) {
      var currentCountry = defaultCountryCode;
      if (countryCodeField) {
        currentCountry = this[countryCodeField] || currentCountry;
      }

      next(
        lodash.isUndefined(value)
          || lodash.isNull(value)
          || validators.isValid(value, currentCountry)
      );
    },
    msg: message
  };
};