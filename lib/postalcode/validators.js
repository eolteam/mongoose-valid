'use strict';

var xregexp = require('xregexp').XRegExp;

module.exports = {
  ad: 'AD\\d{3}', // Andorra
  am: '(37)?\\d{4}', // Armenien
  ar: '([A-HJ-NP-Z])?\\d{4}([A-Z]{3})?', // Argentinien
  as: '96799', // Amerikanisch-Samoa
  at: '\\d{4}', // Österreich
  au: '\\d{4}', // Australien
  ax: '22\\d{3}', // Alandinseln
  az: '\\d{4}', // Aserbaidschan
  ba: '\\d{5}', // Bosnien und Herzegowina
  bb: '(BB\\d{5})?', // Barbados
  bd: '\\d{4}', // Bangladesch
  be: '\\d{4}', // Belgien
  bg: '\\d{4}', // Bulgarien
  bh: '((1[0-2]|[2-9])\\d{2})?', // Bahrain
  bm: '[A-Z]{2}[ ]?[A-Z0-9]{2}', // Bermuda
  bn: '[A-Z]{2}[ ]?\\d{4}', // Brunei Darussalam
  br: '\\d{5}[\\-]?\\d{3}', // Brasilien
  by: '\\d{6}', // Belarus
  ca: '[ABCEGHJKLMNPRSTVXY]\\d[A-Z][ ]?\\d[A-Z]\\d', // Kanada
  cc: '6799', // Kokosinseln
  ch: '\\d{4}', // Schweiz
  ck: '\\d{4}', // Cookinseln
  cl: '\\d{7}', // Chile
  cn: '\\d{6}', // China
  cr: '\\d{4,5}|\\d{3}-\\d{4}', // Costa Rica
  cs: '\\d{5}', // Serbien und Montenegro
  cv: '\\d{4}', // Kap Verde
  cx: '6798', // Weihnachtsinsel
  cy: '\\d{4}', // Zypern
  cz: '\\d{3}[ ]?\\d{2}', // Tschechische Republik
  de: '\\d{5}', // Deutschland
  dk: '\\d{4}', // Dänemark
  do: '\\d{5}', // Dominikanische Republik
  dz: '\\d{5}', // Algerien
  ec: '([A-Z]\\d{4}[A-Z]|(?:[A-Z]{2})?\\d{6})?', // Ecuador
  ee: '\\d{5}', // Estland
  eg: '\\d{5}', // Ägypten
  es: '\\d{5}', // Spanien
  et: '\\d{4}', // Äthiopien
  fi: '\\d{5}', // Finnland
  fk: 'FIQQ 1ZZ', // Falklandinseln
  fm: '(9694[1-4])([ \\-]\\d{4})?', // Mikronesien
  fo: '\\d{3}', // Färöer
  fr: '\\d{2}[ ]?\\d{3}', // Frankreich
  gb: 'GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\\d[\\dA-Z]?[]?\\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\\d{1,4}', // Vereinigtes Königreich
  ge: '\\d{4}', // Georgien
  gf: '9[78]3\\d{2}', // Französisch-Guayana
  gg: 'GY\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}', // Guernsey
  gl: '39\\d{2}', // Grönland
  gn: '\\d{3}', // Guinea
  gp: '9[78][01]\\d{2}', // Guadeloupe
  gr: '\\d{3}[ ]?\\d{2}', // Griechenland
  gs: 'SIQQ 1ZZ', // Südgeorgien und die Südlichen Sandwichinseln
  gt: '\\d{5}', // Guatemala
  gu: '969[123]\\d([ \\-]\\d{4})?', // Guam
  gw: '\\d{4}', // Guinea-Bissau
  hm: '\\d{4}', // Heard- und McDonald-Inseln
  hn: '(?:\\d{5})?', // Honduras
  hr: '\\d{5}', // Kroatien
  ht: '\\d{4}', // Haiti
  hu: '\\d{4}', // Ungarn
  id: '\\d{5}', // Indonesien
  ie: '((D|DUBLIN)?([1-9]|6[wW]|1[0-8]|2[024]))?', // Irland
  il: '\\d{5}', // Israel
  im: 'IM\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}', // Isle of Man
  in: '\\d{6}', // Indien
  io: 'BBND 1ZZ', // Britisches Territorium im Indischen Ozean
  iq: '\\d{5}', // Irak
  is: '\\d{3}', // Island
  it: '\\d{5}', // Italien
  je: 'JE\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}', // Jersey
  jo: '\\d{5}', // Jordanien
  jp: '\\d{3}-\\d{4}', // Japan
  ke: '\\d{5}', // Kenia
  kg: '\\d{6}', // Kirgisistan
  kh: '\\d{5}', // Kambodscha
  kr: '\\d{3}[\\-]\\d{3}', // Republik Korea
  kw: '\\d{5}', // Kuwait
  kz: '\\d{6}', // Kasachstan
  la: '\\d{5}', // Laos
  lb: '(\\d{4}([ ]?\\d{4})?)?', // Libanon
  li: '(948[5-9])|(949[0-7])', // Liechtenstein
  lk: '\\d{5}', // Sri Lanka
  lr: '\\d{4}', // Liberia
  ls: '\\d{3}', // Lesotho
  lt: '\\d{5}', // Litauen
  lu: '\\d{4}', // Luxemburg
  lv: '\\d{4}', // Lettland
  ma: '\\d{5}', // Marokko
  mc: '980\\d{2}', // Monaco
  md: '\\d{4}', // Republik Moldau
  me: '8\\d{4}', // Montenegro
  mg: '\\d{3}', // Madagaskar
  mh: '969[67]\\d([ \\-]\\d{4})?', // Marshallinseln
  mk: '\\d{4}', // Mazedonien
  mn: '\\d{6}', // Mongolei
  mp: '9695[012]([ \\-]\\d{4})?', // Nördliche Marianen
  mq: '9[78]2\\d{2}', // Martinique
  mt: '[A-Z]{3}[ ]?\\d{2,4}', // Malta
  mu: '(\\d{3}[A-Z]{2}\\d{3})?', // Mauritius
  mv: '\\d{5}', // Malediven
  mx: '\\d{5}', // Mexiko
  my: '\\d{5}', // Malaysia
  nc: '988\\d{2}', // Neukaledonien
  ne: '\\d{4}', // Niger
  nf: '2899', // Norfolkinsel
  ng: '(\\d{6})?', // Nigeria
  ni: '((\\d{4}-)?\\d{3}-\\d{3}(-\\d{1})?)?', // Nicaragua
  nl: '\\d{4}[ ]?[A-Z]{2}', // Niederlande
  no: '\\d{4}', // Norwegen
  np: '\\d{5}', // Nepal
  nz: '\\d{4}', // Neuseeland
  om: '(PC )?\\d{3}', // Oman
  pf: '987\\d{2}', // Französisch-Polynesien
  pg: '\\d{3}', // Papua-Neuguinea
  ph: '\\d{4}', // Philippinen
  pk: '\\d{5}', // Pakistan
  pl: '\\d{2}-\\d{3}', // Polen
  pm: '9[78]5\\d{2}', // St. Pierre und Miquelon
  pn: 'PCRN 1ZZ', // Pitcairn
  pr: '00[679]\\d{2}([ \\-]\\d{4})?', // Puerto Rico
  pt: '\\d{4}([\\-]\\d{3})?', // Portugal
  pw: '96940', // Palau
  py: '\\d{4}', // Paraguay
  re: '9[78]4\\d{2}', // Réunion
  ro: '\\d{6}', // Rumänien
  rs: '\\d{6}', // Serbien
  ru: '\\d{6}', // Russische Föderation
  sa: '\\d{5}', // Saudi-Arabien
  se: '\\d{3}[ ]?\\d{2}', // Schweden
  sg: '\\d{6}', // Singapur
  sh: 'STHL 1ZZ', // St. Helena
  si: '\\d{4}', // Slowenien
  sj: '\\d{4}', // Svalbard und Jan Mayen
  sk: '\\d{3}[ ]?\\d{2}', // Slowakei
  sm: '4789\\d', // San Marino
  sn: '\\d{5}', // Senegal
  so: '\\d{5}', // Somalia
  sz: '[HLMS]\\d{3}', // Swasiland
  tc: 'TKCA 1ZZ', // Turks- und Caicosinseln
  th: '\\d{5}', // Thailand
  tj: '\\d{6}', // Tadschikistan
  tm: '\\d{6}', // Turkmenistan
  tn: '\\d{4}', // Tunesien
  tr: '\\d{5}', // Türkei
  tw: '\\d{3}(\\d{2})?', // Taiwan
  ua: '\\d{5}', // Ukraine
  us: '\\d{5}([ \\-]\\d{4})?', // Vereinigte Staaten
  uy: '\\d{5}', // Uruguay
  uz: '\\d{6}', // Usbekistan
  va: '00120', // Vatikanstadt
  ve: '\\d{4}', // Venezuela
  vi: '008(([0-4]\\d)|(5[01]))([ \\-]\\d{4})?', // Amerikanische Jungferninseln
  wf: '986\\d{2}', // Wallis und Futuna
  yt: '976\\d{2}', // Mayotte
  za: '\\d{4}', // Südafrika
  zm: '\\d{5}', // Sambia

  isValid: function (value, countryCode) {
    if (!Object.hasOwnProperty.call(this, countryCode.toLowerCase())) {
      return true;
    }
    return xregexp(this[countryCode.toLowerCase()]).test(value);
  }
};

