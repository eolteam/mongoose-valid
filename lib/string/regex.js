'use strict';

var lodash = require('lodash');
var xregexp = require('xregexp').XRegExp;

module.exports = function (message, regex, ignoreUndef) {
  message = message || ('Regex don\'t match');
  ignoreUndef = ignoreUndef || false;
  regex = xregexp(regex);

  return {
    validator: function (value, next) {
      next(
        (lodash.isUndefined(value) && ignoreUndef) || regex.test(value)
      );
    },
    msg: message
  };
};