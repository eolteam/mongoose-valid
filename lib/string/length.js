'use strict';

module.exports = function (message, min, max) {
  message = message || 'Value length is not in range.';

  return {
    validator: function (value, next) {
      next(
        (!value) || value.length >= min && (!max || value.length <= max)
      );
    },
    msg: message
  };
};