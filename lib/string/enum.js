'use strict';

var lodash = require('lodash');

module.exports = function validateRequired(message, validValues) {
  message = message || 'Value is not a valid value.';

  return {
    validator: function (value, next) {
      next(lodash.isArray(validValues) && lodash.indexOf(validValues, value) !== -1);
    },
    msg: message
  };
};