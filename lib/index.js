module.exports = {};

module.exports.required = require('./required.js');
module.exports.unique = require('./unique.js');
module.exports.string = require('./string');
module.exports.email = require('./email');
module.exports.postalcode = require('./postalcode');