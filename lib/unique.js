'use strict';

module.exports = function validateRequired(message, path) {
  message = message || 'Field should be unique.';

  return {
    validator: function (value, next) {
      var findObject = {};
      findObject[path] = value;

      var model = this.constructor.db.model(this.constructor.modelName);

      if (!this.isModified(path) && !this.isNew) {
        return next(true);
      }

      model.findOne(
        findObject,
        function (err, data) {
          next(!data);
        }
      );
    },
    msg: message
  };
};